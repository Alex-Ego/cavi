import {StyleSheet} from 'react-native';

const style = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  bold: {
    fontWeight: 'bold',
  },
  meaningCont: {
    flex: 1,
    padding: 10,
    backgroundColor: 'lightgray',
  },
});

export default style;
