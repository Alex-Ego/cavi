import React from 'react';
import {ScrollView, View} from 'react-native';
import {
  Text,
  Divider,
  Input,
  Card,
  Button,
  Icon,
  CheckBox,
} from 'react-native-elements';
import {useFormikContext, FieldArray} from 'formik';

import ButtonTab from '../../components/buttonTab/ButtonTab';
import colors from '../../config/colors';
import style from './style';
import i18n from '../../config/i18n';

const {t} = i18n;

const DaycareForm = ({data}) => {
  const {
    handleChange,
    handleSubmit,
    setFieldValue,
    values,
    errors,
  } = useFormikContext();

  return (
    <ScrollView style={style.scontainer}>
      <View style={style.subsection}>
        <Divider style={style.divider} />
        <Text h4>{t.scenes.daycareForm.title}</Text>
      </View>
      <CheckBox
        title={t.schema.daycare.ausencia}
        checked={values.ausencia}
        onPress={() => {
          setFieldValue('ausencia', !values.ausencia);
        }}
      />
      <FieldArray
        name="guarderias"
        render={(arrayHelpers) => (
          <>
            {values.guarderias.map((f, i) => (
              <Card key={i}>
                <Text h5>{t.scenes.daycareForm.subtitle + '# ' + (i + 1)}</Text>
                <Button
                  buttonStyle={style.btnRemove}
                  onPress={() => arrayHelpers.remove(i)}
                  icon={
                    <Icon
                      name="trash"
                      type="font-awesome-5"
                      size={16}
                      color={colors.white}
                    />
                  }
                />
                <Input
                  placeholder={t.schema.daycare.nombre}
                  errorStyle={style.inputError}
                  errorMessage={
                    errors.guarderias && errors.guarderias[i]
                      ? errors.guarderias[i].nombre
                      : ''
                  }
                  onChangeText={handleChange(`guarderias.${i}.nombre`)}
                  value={f.nombre}
                />
                <Input
                  placeholder={t.schema.daycare.telefono}
                  errorStyle={style.inputError}
                  errorMessage={
                    errors.guarderias && errors.guarderias[i]
                      ? errors.guarderias[i].telefono
                      : ''
                  }
                  onChangeText={handleChange(`guarderias.${i}.telefono`)}
                  value={f.telefono}
                />
                <Input
                  placeholder={t.schema.daycare.celular}
                  errorStyle={style.inputError}
                  errorMessage={
                    errors.guarderias && errors.guarderias[i]
                      ? errors.guarderias[i].celular
                      : ''
                  }
                  onChangeText={handleChange(`guarderias.${i}.celular`)}
                  value={f.celular}
                />
              </Card>
            ))}
            <Card>
              <Button
                title={t.scenes.daycareForm.add + ' '}
                onPress={() => arrayHelpers.push('')}
                buttonStyle={{backgroundColor: colors.secondary}}
                iconRight
                icon={
                  <Icon
                    name="plus"
                    type="font-awesome-5"
                    size={16}
                    color={colors.white}
                  />
                }
              />
            </Card>
          </>
        )}
      />
      <ButtonTab title={t.shared.next} onPress={() => handleSubmit()} />
    </ScrollView>
  );
};

export default DaycareForm;
