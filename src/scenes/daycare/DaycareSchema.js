import * as yup from 'yup';

import i18n from '../../config/i18n';

const {t} = i18n;

export const initialDaycareValues = {
  guarderias: [
    {
      nombre: '',
      telefono: '',
      celular: '',
    },
  ],
};

export const DaycareSchema = yup.object().shape({
  guarderias: yup.array().of(
    yup.object().shape({
      nombre: yup.string().max(80, t.shared.max).required(t.shared.required),
      telefono: yup.number().required(t.shared.required),
      celular: yup.number().required(t.shared.required),
    }),
  ),
});
