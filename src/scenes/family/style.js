import {StyleSheet} from 'react-native';
import colors from '../../config/colors';

const style = StyleSheet.create({
  container: {
    flex: 1,
  },
  scontainer: {
    flex: 1,
    margin: 5,
  },
  subsection: {
    margin: 10,
  },
  inputError: {
    color: colors.error,
  },
  divider: {
    backgroundColor: colors.secondary,
  },
  addIcon: {
    color: colors.primary,
  },
  btnRemove: {
    backgroundColor: colors.error,
    width: 30,
    alignSelf: 'flex-end',
  },
});

export default style;
