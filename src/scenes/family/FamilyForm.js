import React from 'react';
import {ScrollView, View} from 'react-native';
import {
  Text,
  Divider,
  Input,
  Card,
  Button,
  Icon,
  CheckBox,
} from 'react-native-elements';
import {useFormikContext, FieldArray} from 'formik';

import ButtonTab from '../../components/buttonTab/ButtonTab';
import colors from '../../config/colors';
import style from './style';
import i18n from '../../config/i18n';

const {t} = i18n;

const FamilyForm = ({data}) => {
  const {
    handleChange,
    handleSubmit,
    setFieldValue,
    values,
    errors,
  } = useFormikContext();

  return (
    <ScrollView style={style.scontainer}>
      <View style={style.subsection}>
        <Divider style={style.divider} />
        <Text h4>{t.scenes.familyForm.title}</Text>
      </View>
      <FieldArray
        name="familiares"
        render={(arrayHelpers) => (
          <>
            {values.familiares.map((f, i) => (
              <Card key={i}>
                <Text h5>{t.scenes.familyForm.subtitle + '# ' + (i + 1)}</Text>
                <Button
                  buttonStyle={style.btnRemove}
                  onPress={() => arrayHelpers.remove(i)}
                  icon={
                    <Icon
                      name="trash"
                      type="font-awesome-5"
                      size={16}
                      color={colors.white}
                    />
                  }
                />
                <Input
                  placeholder={t.schema.family.nombre}
                  errorStyle={style.inputError}
                  errorMessage={
                    errors.familiares && errors.familiares[i]
                      ? errors.familiares[i].nombre
                      : ''
                  }
                  onChangeText={handleChange(`familiares.${i}.nombre`)}
                  value={f.nombre}
                />
                <Input
                  placeholder={t.schema.family.parentesco}
                  errorStyle={style.inputError}
                  errorMessage={
                    errors.familiares && errors.familiares[i]
                      ? errors.familiares[i].parentesco
                      : ''
                  }
                  onChangeText={handleChange(`familiares.${i}.parentesco`)}
                  value={f.parentesco}
                />
                <Input
                  placeholder={t.schema.family.edad}
                  errorStyle={style.inputError}
                  errorMessage={
                    errors.familiares && errors.familiares[i]
                      ? errors.familiares[i].edad
                      : ''
                  }
                  onChangeText={handleChange('familiares.${i}.edad')}
                  value={f.edad}
                />
                <Input
                  placeholder={t.schema.family.ultimoNivel}
                  errorStyle={style.inputError}
                  errorMessage={
                    errors.familiares && errors.familiares[i]
                      ? errors.familiares[i].ultimoNivel
                      : ''
                  }
                  onChangeText={handleChange(`familiares.${i}.ultimoNivel`)}
                  value={f.ultimoNivel}
                />
                <Input
                  placeholder={t.schema.family.ocupacion}
                  errorStyle={style.inputError}
                  errorMessage={
                    errors.familiares && errors.familiares[i]
                      ? errors.familiares[i].ocupacion
                      : ''
                  }
                  onChangeText={handleChange(`familiares.${i}.ocupacion`)}
                  value={f.ocupacion}
                />
                <Input
                  placeholder={t.schema.family.dondeTrabaja}
                  errorStyle={style.inputError}
                  errorMessage={
                    errors.familiares && errors.familiares[i]
                      ? errors.familiares[i].dondeTrabaja
                      : ''
                  }
                  onChangeText={handleChange(`familiares.${i}.dondeTrabaja`)}
                  value={f.dondeTrabaja}
                />
                <Input
                  placeholder={t.schema.family.imss}
                  errorStyle={style.inputError}
                  errorMessage={
                    errors.familiares && errors.familiares[i]
                      ? errors.familiares[i].imss
                      : ''
                  }
                  onChangeText={handleChange(`familiares.${i}.imss`)}
                  value={f.imss}
                />
                <Input
                  placeholder={t.schema.family.ingresoMensual}
                  errorStyle={style.inputError}
                  errorMessage={
                    errors.familiares && errors.familiares[i]
                      ? errors.familiares[i].ingresoMensual
                      : ''
                  }
                  onChangeText={handleChange(`familiares.${i}.ingresoMensual`)}
                  value={f.ingresoMensual}
                />
                <CheckBox
                  title={t.schema.family.padeceDiscapacidad}
                  checked={f.padeceDiscapacidad}
                  onPress={() => {
                    setFieldValue(
                      `familiares.${i}.padeceDiscapacidad`,
                      !f.padeceDiscapacidad,
                    );
                  }}
                />
                <Input
                  placeholder={t.schema.family.queDiscapacidad}
                  errorStyle={style.inputError}
                  errorMessage={
                    errors.familiares && errors.familiares[i]
                      ? errors.familiares[i].queDiscapacidad
                      : ''
                  }
                  onChangeText={handleChange(`familiares.${i}.queDiscapacidad`)}
                  value={f.queDiscapacidad}
                />
                <CheckBox
                  title={t.schema.family.padeceEnfermedad}
                  checked={f.padeceEnfermedad}
                  onPress={() => {
                    setFieldValue(
                      `familiares.${i}.padeceEnfermedad`,
                      !f.padeceEnfermedad,
                    );
                  }}
                />
                <Input
                  placeholder={t.schema.family.queEnfermedad}
                  errorStyle={style.inputError}
                  errorMessage={
                    errors.familiares && errors.familiares[i]
                      ? errors.familiares[i].queEnfermedad
                      : ''
                  }
                  onChangeText={handleChange(`familiares.${i}.queEnfermedad`)}
                  value={f.queEnfermedad}
                />
              </Card>
            ))}
            <Card>
              <Button
                title={t.scenes.familyForm.add + ' '}
                onPress={() => arrayHelpers.push('')}
                buttonStyle={{backgroundColor: colors.secondary}}
                iconRight
                icon={
                  <Icon
                    name="plus"
                    type="font-awesome-5"
                    size={16}
                    color={colors.white}
                  />
                }
              />
            </Card>
          </>
        )}
      />
      <ButtonTab title={t.shared.next} onPress={() => handleSubmit()} />
    </ScrollView>
  );
};

export default FamilyForm;
