import * as yup from 'yup';

import i18n from '../../config/i18n';

const {t} = i18n;

export const initialFamilyValues = {
  familiares: [
    {
      nombre: '',
      parentesco: '',
      edad: '',
      ultimoNivel: '',
      ocupacion: '',
      dondeTrabaja: '',
      imss: '',
      ingresoMensual: '',
      padeceDiscapacidad: false,
      padeceEnfermedad: false,
      queDiscapacidad: '',
      queEnfermedad: '',
    },
  ],
};

export const FamilySchema = yup.object().shape({
  familiares: yup.array().of(
    yup.object().shape({
      nombre: yup.string().max(80, t.shared.max).required(t.shared.required),
      parentesco: yup.string().max(15, t.shared.max).required(t.shared.required),
      edad: yup.number().required(t.shared.required),
      ultimoNivel: yup.string().max(15, t.shared.max).required(t.shared.required),
      ocupacion: yup.string().max(20, t.shared.max).required(t.shared.required),
      dondeTrabaja: yup.string().max(20, t.shared.max).required(t.shared.required),
      imss: yup.string().max(20, t.shared.max),
      ingresoMensual: yup.number().required(t.shared.required),
    }),
  ),
});
