import * as yup from 'yup';

import i18n from '../../config/i18n';

const {t} = i18n;

export const initialPersonValues = {
  nombre: '',
  apellidoP: '',
  apellidoM: '',
  fechaNacimiento: '',
  paisNacimiento: '',
  estadoNacimiento: '',
  ciudadNacimiento: '',
  estadoCivil: '',
  telefono: '',
  celular: '',
  correo: '',
  sexo: '',
  curp: '',
  calle: '',
  numero: '',
  colonia: '',
  ciudad: '',
  codigoPostal: '',
};

export const PersonalSchema = yup.object({
  nombre: yup.string().max(15, t.shared.max).required(t.shared.required),
  apellidoP: yup.string().max(15, t.shared.max).required(t.shared.required),
  apellidoM: yup.string().max(15, t.shared.max).required(t.shared.required),
  fechaNacimiento: yup.date().required(t.shared.required),
  paisNacimiento: yup.string().required(t.shared.required),
  estadoNacimiento: yup.string().required(t.shared.required),
  ciudadNacimiento: yup.string().required(t.shared.required),
  estadoCivil: yup.string().required(t.shared.required),
  telefono: yup.number().required(t.shared.required),
  celular: yup.number().required(t.shared.required),
  correo: yup.string().email().required(t.shared.required),
  sexo: yup.string().required(t.shared.required),
  curp: yup.string().required(t.shared.required),
  calle: yup.string().required(t.shared.required),
  numero: yup.string().required(t.shared.required),
  colonia: yup.string().required(t.shared.required),
  ciudad: yup.string().required(t.shared.required),
  codigoPostal: yup.string().required(t.shared.required),
});
