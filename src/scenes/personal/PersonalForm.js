import React, {useState} from 'react';
import {ScrollView, View, Platform, Keyboard} from 'react-native';
import {Text, Divider, Input} from 'react-native-elements';
import {useFormikContext} from 'formik';
import moment from 'moment';
import DateTimePicker from '@react-native-community/datetimepicker';
import {Picker} from '@react-native-community/picker';

import ButtonTab from '../../components/buttonTab/ButtonTab';
import style from './style';
import i18n from '../../config/i18n';

const {t} = i18n;

const PersonalForm = () => {
  const {
    handleChange,
    handleSubmit,
    setFieldValue,
    values,
    errors,
  } = useFormikContext();

  const [date, setDate] = useState(new Date(1598051730000));
  const [show, setShow] = useState(false);
  const [paisP, setPaisP] = useState(null);
  const [estadoP, setEstadoP] = useState(null);
  const [estadoCivilP, setEstadoCivilP] = useState(null);
  const [sexoP, setSexoP] = useState(null);

  const DP_onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
    setFieldValue('fechaNacimiento', moment(date).format('l'));
  };

  const showDatepicker = () => {
    setShow(true);
  };

  return (
    <ScrollView style={style.scontainer}>
      <View style={style.subsection}>
        <Divider style={style.divider} />
        <Text h4>{t.scenes.personalForm.title}</Text>
      </View>
      <Input
        placeholder={t.schema.person.nombre}
        errorStyle={style.inputError}
        errorMessage={errors.nombre}
        onChangeText={handleChange('nombre')}
        value={values.nombre}
      />
      <Input
        placeholder={t.schema.person.apellidoP}
        errorStyle={style.inputError}
        errorMessage={errors.apellidoP}
        onChangeText={handleChange('apellidoP')}
        value={values.apellidoP}
      />
      <Input
        placeholder={t.schema.person.apellidoM}
        errorStyle={style.inputError}
        errorMessage={errors.apellidoM}
        onChangeText={handleChange('apellidoM')}
        value={values.apellidoM}
      />
      <Input
        placeholder={t.schema.person.fechaNacimiento}
        errorStyle={style.inputError}
        onFocus={Keyboard.dismiss}
        onTouchStart={showDatepicker}
        errorMessage={errors.fechaNacimiento}
        onChangeText={handleChange('fechaNacimiento')}
        value={values.fechaNacimiento}
      />
      {show && (
        <DateTimePicker value={date} mode="date" onChange={DP_onChange} />
      )}
      <Picker
        selectedValue={paisP}
        onValueChange={(v) => {
          setFieldValue('paisNacimiento', v);
          setPaisP(v);
        }}>
        <Picker.Item label={t.schema.person.paisNacimiento} value="" />
        <Picker.Item label="México" value="México" />
      </Picker>
      <Picker
        selectedValue={estadoP}
        onValueChange={(v) => {
          setFieldValue('estadoNacimiento', v);
          setEstadoP(v);
        }}>
        <Picker.Item label={t.schema.person.estadoNacimiento} value="" />
        <Picker.Item label="Nuevo Leon" value="Nuevo Leon" />
      </Picker>
      <Input
        placeholder={t.schema.person.ciudadNacimiento}
        errorStyle={style.inputError}
        errorMessage={errors.ciudadNacimiento}
        onChangeText={handleChange('ciudadNacimiento')}
        value={values.ciudadNacimiento}
      />
      <Picker
        selectedValue={estadoCivilP}
        onValueChange={(v) => {
          setFieldValue('estadoCivil', v);
          setEstadoCivilP(v);
        }}>
        <Picker.Item label={t.schema.person.estadoCivil} value="" />
        <Picker.Item label="Union Libre" value="Union Libre" />
        <Picker.Item label="Divorciada" value="Divorciada" />
        <Picker.Item label="Soltera" value="Soltera" />
        <Picker.Item label="Casada" value="Casada" />
        <Picker.Item label="Viuda" value="Viuda" />
        <Picker.Item label="Separada" value="Separada" />
      </Picker>
      <Input
        placeholder={t.schema.person.telefono}
        errorStyle={style.inputError}
        errorMessage={errors.telefono}
        onChangeText={handleChange('telefono')}
        value={values.telefono}
      />
      <Input
        placeholder={t.schema.person.celular}
        errorStyle={style.inputError}
        errorMessage={errors.celular}
        onChangeText={handleChange('celular')}
        value={values.celular}
      />
      <Input
        placeholder={t.schema.person.correo}
        errorStyle={style.inputError}
        errorMessage={errors.correo}
        onChangeText={handleChange('correo')}
        value={values.correo}
      />
      <Picker
        selectedValue={sexoP}
        onValueChange={(v) => {
          setFieldValue('sexo', v);
          setSexoP(v);
        }}>
        <Picker.Item label={t.schema.person.sexo} value="" />
        <Picker.Item label="Femenino" value="Femenino" />
        <Picker.Item label="Masculino" value="Masculino" />
      </Picker>
      <Input
        placeholder={t.schema.person.curp}
        errorStyle={style.inputError}
        errorMessage={errors.curp}
        onChangeText={handleChange('curp')}
        value={values.curp}
      />
      <View style={style.subsection}>
        <Divider style={style.divider} />
        <Text h4>{t.scenes.personalForm.subtitle}</Text>
      </View>
      <Input
        placeholder={t.schema.person.calle}
        errorStyle={style.inputError}
        errorMessage={errors.calle}
        onChangeText={handleChange('calle')}
        value={values.calle}
      />
      <Input
        placeholder={t.schema.person.numero}
        errorStyle={style.inputError}
        errorMessage={errors.numero}
        onChangeText={handleChange('numero')}
        value={values.numero}
      />
      <Input
        placeholder={t.schema.person.colonia}
        errorStyle={style.inputError}
        errorMessage={errors.colonia}
        onChangeText={handleChange('colonia')}
        value={values.colonia}
      />
      <Input
        placeholder={t.schema.person.ciudad}
        errorStyle={style.inputError}
        errorMessage={errors.ciudad}
        onChangeText={handleChange('ciudad')}
        value={values.ciudad}
      />
      <Input
        placeholder={t.schema.person.codigoPostal}
        errorStyle={style.inputError}
        errorMessage={errors.codigoPostal}
        onChangeText={handleChange('codigoPostal')}
        value={values.codigoPostal}
      />
      <ButtonTab title={t.shared.next} onPress={() => handleSubmit()} />
    </ScrollView>
  );
};

export default PersonalForm;
