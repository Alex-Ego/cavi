import React, {useState, useEffect} from 'react';
import {ScrollView, View} from 'react-native';
import {Text, Divider, Input, CheckBox} from 'react-native-elements';
import {useFormikContext} from 'formik';
import {Picker} from '@react-native-community/picker';

import ButtonTab from '../../components/buttonTab/ButtonTab';
import style from './style';
import i18n from '../../config/i18n';

const {t} = i18n;

const EconomicForm = (props) => {
  const {
    handleChange,
    handleSubmit,
    setFieldValue,
    values,
    errors,
  } = useFormikContext();

  const [nombreServMedicoP, setNombreServMedicoP] = useState(null);
  const [tipoAguaPotP, setTipoAguaPotP] = useState(null);
  const [tipoVivP, setTipoVivP] = useState(null);
  const [tipoCombustP, setTipoCombustP] = useState(null);
  const [pisoVivP, setPisoVivP] = useState(null);
  const [techoVivP, setTechoVivP] = useState(null);
  const [murosVivP, setMurosVivP] = useState(null);

  return (
    <ScrollView style={style.scontainer}>
      <View style={style.subsection}>
        <Divider style={style.divider} />
        <Text h4>{t.scenes.economicForm.subtitle1}</Text>
      </View>
      <CheckBox
        title={t.schema.economic.primaria}
        checked={values.primaria}
        onPress={() => {
          setFieldValue('primaria', !values.primaria);
        }}
      />
      <CheckBox
        title={t.schema.economic.secundaria}
        checked={values.secundaria}
        onPress={() => {
          setFieldValue('secundaria', !values.secundaria);
        }}
      />
      <Input
        placeholder={t.schema.economic.ultimoNivel}
        errorStyle={style.inputError}
        errorMessage={errors.ultimoNivel}
        onChangeText={handleChange('ultimoNivel')}
        value={values.ultimoNivel}
      />
      <View style={style.subsection}>
        <Divider style={style.divider} />
        <Text h4>{t.scenes.economicForm.subtitle2}</Text>
      </View>
      <CheckBox
        title={t.schema.economic.servMedico}
        checked={values.servMedico}
        onPress={() => {
          setFieldValue('servMedico', !values.servMedico);
        }}
      />
      <Picker
        selectedValue={nombreServMedicoP}
        onValueChange={(v, i) => {
          setFieldValue('nombreServMedico', v);
          setNombreServMedicoP(v);
        }}>
        <Picker.Item label={t.schema.economic.nombreServMedico} value="" />
        <Picker.Item label="IMSS" value="IMSS" />
        <Picker.Item label="ISSTE" value="ISSTE" />
        <Picker.Item label="SEGURO POPULAR" value="SEGURO POPULAR" />
        <Picker.Item label="ISSSTE LEON" value="ISSSTE LEON" />
        <Picker.Item
          label="SEGURO DE GASTOS MEDICOS"
          value="SEGURO DE GASTOS MEDICOS"
        />
      </Picker>
      <Input
        placeholder={t.schema.economic.titularIMSS}
        errorStyle={style.inputError}
        errorMessage={errors.titularIMSS}
        onChangeText={handleChange('titularIMSS')}
        value={values.titularIMSS}
      />
      <CheckBox
        title={t.schema.economic.enfermedadActual}
        checked={values.enfermedadActual}
        onPress={() => {
          setFieldValue('enfermedadActual', !values.enfermedadActual);
        }}
      />
      <CheckBox
        title={t.schema.economic.medicamento}
        checked={values.medicamento}
        onPress={() => {
          setFieldValue('medicamento', !values.medicamento);
        }}
      />
      <Input
        placeholder={t.schema.economic.peso}
        errorStyle={style.inputError}
        errorMessage={errors.peso}
        onChangeText={handleChange('peso')}
        value={values.peso}
      />
      <Input
        placeholder={t.schema.economic.altura}
        errorStyle={style.inputError}
        errorMessage={errors.altura}
        onChangeText={handleChange('altura')}
        value={values.altura}
      />
      <CheckBox
        title={t.schema.economic.alergia}
        checked={values.alergia}
        onPress={() => {
          setFieldValue('alergia', !values.alergia);
        }}
      />
      <Input
        placeholder={t.schema.economic.cualAlergia}
        errorStyle={style.inputError}
        errorMessage={errors.cualAlergia}
        onChangeText={handleChange('cualAlergia')}
        value={values.cualAlergia}
      />
      <CheckBox
        title={t.schema.economic.doctor}
        checked={values.doctor}
        onPress={() => {
          setFieldValue('doctor', !values.doctor);
        }}
      />
      <Input
        placeholder={t.schema.economic.nombreDoctor}
        errorStyle={style.inputError}
        errorMessage={errors.nombreDoctor}
        onChangeText={handleChange('nombreDoctor')}
        value={values.nombreDoctor}
      />
      <Input
        placeholder={t.schema.economic.espDoctor}
        errorStyle={style.inputError}
        errorMessage={errors.espDoctor}
        onChangeText={handleChange('espDoctor')}
        value={values.espDoctor}
      />
      <Input
        placeholder={t.schema.economic.telDoctor}
        errorStyle={style.inputError}
        errorMessage={errors.telDoctor}
        onChangeText={handleChange('telDoctor')}
        value={values.telDoctor}
      />
      <View style={style.subsection}>
        <Divider style={style.divider} />
        <Text h4>{i18n.t.scenes.economicForm.subtitle3}</Text>
      </View>
      <Picker
        selectedValue={tipoVivP}
        onValueChange={(v, i) => {
          setFieldValue('tipoViv', v);
          setTipoVivP(v);
        }}>
        <Picker.Item label={t.schema.economic.tipoViv} value="" />
        <Picker.Item label="Propia" value="Propia" />
        <Picker.Item label="Rentada" value="Rentada" />
        <Picker.Item label="Prestada" value="Prestada" />
        <Picker.Item label="Posesionario" value="Posesionario" />
        <Picker.Item label="Familiar" value="Familiar" />
      </Picker>
      <Input
        placeholder={t.schema.economic.habitacionesViv}
        errorStyle={style.inputError}
        errorMessage={errors.habitacionesViv}
        onChangeText={handleChange('habitacionesViv')}
        value={values.habitacionesViv}
      />
      <Input
        placeholder={t.schema.economic.personasPorHab}
        errorStyle={style.inputError}
        errorMessage={errors.personasPorHab}
        onChangeText={handleChange('personasPorHab')}
        value={values.personasPorHab}
      />
      <Input
        placeholder={t.schema.economic.banoViv}
        errorStyle={style.inputError}
        errorMessage={errors.banoViv}
        onChangeText={handleChange('banoViv')}
        value={values.banoViv}
      />
      <Picker
        selectedValue={pisoVivP}
        onValueChange={(v, i) => {
          setFieldValue('pisoViv', v);
          setPisoVivP(v);
        }}>
        <Picker.Item label={t.schema.economic.pisoViv} value="" />
        <Picker.Item label="Tierra" value="Tierra" />
        <Picker.Item label="Cemento" value="Cemento" />
        <Picker.Item label="Laminado" value="Laminado" />
        <Picker.Item label="Linoleum o Vinil" value="Linoleum o Vinil" />
        <Picker.Item label="Mosaico o Vitropiso" value="Mosaico o Vitropiso" />
        <Picker.Item label="Otro" value="Otro" />
      </Picker>
      <Picker
        selectedValue={techoVivP}
        onValueChange={(v, i) => {
          setFieldValue('techoViv', v);
          setTechoVivP(v);
        }}>
        <Picker.Item label={t.schema.economic.techoViv} value="" />
        <Picker.Item label="Material de desecho" value="Material de desecho" />
        <Picker.Item label="Lámina de cartón" value="Lámina de cartón" />
        <Picker.Item label="Lámina metálica" value="Lámina metálica" />
        <Picker.Item label="Lámina de asbezto" value="Lámina de asbezto" />
        <Picker.Item label="Madera o tejamanil" value="Madera o tejamanil" />
        <Picker.Item label="Teja" value="Teja" />
        <Picker.Item label="Losa de concreto" value="Losa de concreto" />
        <Picker.Item label="Otro" value="Otro" />
      </Picker>
      <Picker
        selectedValue={murosVivP}
        onValueChange={(v, i) => {
          setFieldValue('murosViv', v);
          setMurosVivP(v);
        }}>
        <Picker.Item label={t.schema.economic.murosViv} value="" />
        <Picker.Item label="Material de desecho" value="Material de desecho" />
        <Picker.Item label="Lámina de cartón" value="Lámina de cartón" />
        <Picker.Item label="Lámina metálica" value="Lámina metálica" />
        <Picker.Item
          label="Carrizo, bambú o palma"
          value="Carrizo, bambú o palma"
        />
        <Picker.Item label="Madera" value="Madera" />
        <Picker.Item label="Block o Concreto" value="Block o concreto" />
        <Picker.Item label="Otro" value="Otro" />
      </Picker>
      <CheckBox
        title={t.schema.economic.aguaPot}
        checked={values.aguaPot}
        onPress={() => {
          setFieldValue('aguaPot', !values.aguaPot);
        }}
      />
      <Picker
        selectedValue={tipoAguaPotP}
        onValueChange={(v, i) => {
          setFieldValue('tipoAguaPot', v);
          setTipoAguaPotP(v);
        }}>
        <Picker.Item label={t.schema.economic.tipoAguaPot} value="" />
        <Picker.Item label="Agua y Drenaje" value="Agua y Drenaje" />
        <Picker.Item label="Pipa" value="Pipa" />
        <Picker.Item label="Otro" value="Otro" />
      </Picker>
      <CheckBox
        title={t.schema.economic.drenaje}
        checked={values.drenaje}
        onPress={() => {
          setFieldValue('drenaje', !values.drenaje);
        }}
      />
      <CheckBox
        title={t.schema.economic.elect}
        checked={values.elect}
        onPress={() => {
          setFieldValue('elect', !values.elect);
        }}
      />
      <CheckBox
        title={t.schema.economic.combust}
        checked={values.combust}
        onPress={() => {
          setFieldValue('combust', !values.combust);
        }}
      />
      <Picker
        selectedValue={tipoCombustP}
        onValueChange={(v, i) => {
          setFieldValue('tipoCombust', v);
          setTipoCombustP(v);
        }}>
        <Picker.Item label={t.schema.economic.tipoCombust} value="" />
        <Picker.Item label="Gas Natural" value="Gas Natural" />
        <Picker.Item label="Gas de Tanque" value="Gas de Tanque" />
        <Picker.Item label="Otro" value="Otro" />
      </Picker>
      <CheckBox
        title={t.schema.economic.pc}
        checked={values.pc}
        onPress={() => {
          setFieldValue('pc', !values.pc);
        }}
      />
      <CheckBox
        title={t.schema.economic.trabajo}
        checked={values.trabajo}
        onPress={() => {
          setFieldValue('trabajo', !values.trabajo);
        }}
      />
      <Input
        placeholder={t.schema.economic.edadEmbarazo}
        errorStyle={style.inputError}
        errorMessage={errors.edadEmbarazo}
        onChangeText={handleChange('edadEmbarazo')}
        value={values.edadEmbarazo}
      />
      <Input
        placeholder={t.schema.economic.primerMat}
        errorStyle={style.inputError}
        errorMessage={errors.primerMat}
        onChangeText={handleChange('primerMat')}
        value={values.primerMat}
      />
      <ButtonTab title={t.shared.next} onPress={() => handleSubmit()} />
    </ScrollView>
  );
};

export default EconomicForm;
