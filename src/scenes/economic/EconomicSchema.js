import * as yup from 'yup';

import i18n from '../../config/i18n';

const {t} = i18n;

export const initialEconomicValues = {
  primaria: false,
  secundaria: false,
  ultimoNivel: '',
  servMedico: false,
  nombreServMedico: '',
  titularIMSS: '',
  enfermedadActual: false,
  medicamento: false,
  peso: '',
  altura: '',
  alergia: false,
  cualAlergia: '',
  doctor: false,
  nombreDoctor: '',
  espDoctor: '',
  telDoctor: '',
  tipoViv: '',
  habitacionesViv: '',
  personasPorHab: '',
  banoViv: '',
  pisoViv: '',
  techoViv: '',
  murosViv: '',
  aguaPot: false,
  tipoAguaPot: '',
  drenaje: false,
  elect: false,
  combust: false,
  tipoCombust: '',
  pc: false,
  trabajo: false,
  edadEmbarazo: '',
  primerMat: '',
};

export const EconomicSchema = yup.object({});
