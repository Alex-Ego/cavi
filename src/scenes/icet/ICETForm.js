import React, {useState} from 'react';
import {ScrollView, View} from 'react-native';
import {Text, Divider, Input, CheckBox} from 'react-native-elements';
import {useFormikContext} from 'formik';
import {Picker} from '@react-native-community/picker';

import ButtonTab from '../../components/buttonTab/ButtonTab';
import style from './style';
import i18n from '../../config/i18n';

const {t} = i18n;

const ICETForm = ({data}) => {
  const {
    handleChange,
    handleSubmit,
    setFieldValue,
    values,
    errors,
  } = useFormikContext();

  const [tallerP, setTallerP] = useState(null);
  const [discapacidadP, setDiscapacidadP] = useState(null);
  const [ultimoNivelP, setUltimoNivelP] = useState(null);
  const [situacionTrabajoP, setSituacionTrabajoP] = useState(null);
  const [ocupacionPrincipalP, setOcupacionPrincipalP] = useState(null);
  const [situacionActualP, setSituacionActualP] = useState(null);
  const [liquidarCursoP, setLiquidarCursoP] = useState(null);

  return (
    <ScrollView style={style.scontainer}>
      <View style={style.subsection}>
        <Divider style={style.divider} />
        <Text h4>{t.scenes.ICETForm.title}</Text>
      </View>
      <Picker
        selectedValue={tallerP}
        onValueChange={(v, i) => {
          setFieldValue('taller', v);
          setTallerP(v);
        }}>
        <Picker.Item label={t.schema.icet.taller} value="" />
        <Picker.Item label="Peluquero - Barbero" value="Peluquero - Barbero" />
        <Picker.Item
          label="Corte y Secado Para Dama"
          value="Corte y secado para dama"
        />
        <Picker.Item
          label="Maquillaje y Peinado Social"
          value="Maquillaje y Peinado Social"
        />
        <Picker.Item
          label="Cuidado y Decoración de Uñas"
          value="Cuidado y decoración de uñas"
        />
        <Picker.Item label="Asistente Educativo" value="Asistente Educativo" />
        <Picker.Item
          label="Cuidado Integral del Adulto Mayor"
          value="Cuidado Integral del Adulto Mayor"
        />
        <Picker.Item label="Colorimetría" value="Colorimetría" />
        <Picker.Item
          label="Pastelería - Repostería"
          value="Pastelería - Repostería"
        />
        <Picker.Item label="Corte y Confección" value="Corte y Confección" />
        <Picker.Item
          label="Diseño e Impresión 3D"
          value="Diseño e Impresión 3D"
        />
      </Picker>
      <Picker
        selectedValue={discapacidadP}
        onValueChange={(v, i) => {
          setFieldValue('discapacidad', v);
          setDiscapacidadP(v);
        }}>
        <Picker.Item label={t.schema.icet.discapacidad} value="" />
        <Picker.Item label="Ninguna" value="Ninguna" />
        <Picker.Item label="Visual" value="Visual" />
        <Picker.Item label="Auditiva" value="Auditiva" />
        <Picker.Item label="De Lenguaje" value="De Lenguaje" />
        <Picker.Item
          label="Motriz o Músculoesq."
          value="Motriz o Músculoesq."
        />
        <Picker.Item label="Mental" value="Mental" />
      </Picker>
      <Picker
        selectedValue={ultimoNivelP}
        onValueChange={(v, i) => {
          setFieldValue('ultimoNivel', v);
          setUltimoNivelP(v);
        }}>
        <Picker.Item label={t.schema.icet.ultimoNivel} value="" />
        <Picker.Item label="Ninguna" value="Ninguna" />
        <Picker.Item label="Primaria inconclusa" value="Primaria inconclusa" />
        <Picker.Item label="Primaria terminada" value="Primaria terminada" />
        <Picker.Item
          label="Secundaria inconclusa"
          value="Secundaria inconclusa"
        />
        <Picker.Item
          label="Secundaria terminada"
          value="Secundaria terminada"
        />
        <Picker.Item
          label="Preparatoria inconclusa"
          value="Preparatoria inconclusa"
        />
        <Picker.Item
          label="Preparatoria completa"
          value="Preparatoria terminada"
        />
        <Picker.Item label="Técnica inconclusa" value="Técnica inconclusa" />
        <Picker.Item label="Técnica terminada" value="Técnica terminada" />

        <Picker.Item
          label="Técnico Sup. inconclusa"
          value="Técnico Sup. inconclusa"
        />
        <Picker.Item
          label="Técnico Sup. terminada"
          value="Técnico Sup. terminada"
        />

        <Picker.Item label="Lic. inconclusa" value="Lic. inconclusa" />
        <Picker.Item label="Lic. terminada" value="Lic. terminada" />
        <Picker.Item label="Posgrado" value="Posgrado" />
      </Picker>
      <CheckBox
        title={t.schema.icet.trabajaActualmente}
        checked={values.trabajaActualmente}
        onPress={() => {
          setFieldValue('trabajaActualmente', !values.trabajaActualmente);
        }}
      />
      <Input
        placeholder={t.schema.icet.nombreEmpresa}
        errorStyle={style.inputError}
        errorMessage={errors.nombreEmpresa}
        onChangeText={handleChange('nombreEmpresa')}
        value={values.nombreEmpresa}
      />
      <Input
        placeholder={t.schema.icet.direccionTrabajo}
        errorStyle={style.inputError}
        errorMessage={errors.direccionTrabajo}
        onChangeText={handleChange('direccionTrabajo')}
        value={values.direccionTrabajo}
      />
      <Input
        placeholder={t.schema.icet.telefonoEmpresa}
        errorStyle={style.inputError}
        errorMessage={errors.telefonoEmpresa}
        onChangeText={handleChange('telefonoEmpresa')}
        value={values.telefonoEmpresa}
      />
      <Input
        placeholder={t.schema.icet.antiguedad}
        errorStyle={style.inputError}
        errorMessage={errors.antiguedad}
        onChangeText={handleChange('antiguedad')}
        value={values.antiguedad}
      />
      <Picker
        selectedValue={situacionTrabajoP}
        onValueChange={(v, i) => {
          setFieldValue('situacionTrabajo', v);
          setSituacionTrabajoP(v);
        }}>
        <Picker.Item label={t.schema.icet.situacionTrabajo} value="" />
        <Picker.Item label="No especificado" value="No especificado" />
        <Picker.Item label="Empleado u obrero" value="Empleado u obrero" />
        <Picker.Item label="Jornalero o peón" value="Jornalero o peón" />
        <Picker.Item
          label="Trabajador por su cuenta"
          value="Trabajador por su cuenta"
        />
        <Picker.Item label="Patrón o empresario" value="Patrón o empresario" />
        <Picker.Item
          label="Trabajador Familiar sin pago"
          value="Trabajador Familiar sin pago"
        />
      </Picker>
      <Picker
        selectedValue={ocupacionPrincipalP}
        onValueChange={(v, i) => {
          setFieldValue('ocupacionPrincipal', v);
          setOcupacionPrincipalP(v);
        }}>
        <Picker.Item label={t.schema.icet.ocupacionPrincipal} value="" />
        <Picker.Item label="No especificado" value="No especificado" />
        <Picker.Item label="Profesionistas" value="Profesionistas" />
        <Picker.Item label="Técnicos" value="Técnicos" />
        <Picker.Item
          label="Trabajador de la Educación"
          value="Trabajador de la Educación"
        />
        <Picker.Item label="Trabajador del Arte" value="Trabajador del Arte" />
        <Picker.Item
          label="Funcionarios y Directivos"
          value="Funcionarios y Directivos"
        />
        <Picker.Item
          label="Trabajador agropecuario"
          value="Trabajador agropecuario"
        />
        <Picker.Item
          label="Inspectores y Supervisores"
          value="Inspectores y Supervisores"
        />
        <Picker.Item label="Artesanos y Obreros" value="Artesanos y Obreros" />
        <Picker.Item
          label="Op. de Máquina Industrial"
          value="Op. de Máquina Industrial"
        />
        <Picker.Item
          label="Ayudantes y Similares"
          value="Ayudantes y Similares"
        />
        <Picker.Item
          label="Operadores de Transporte"
          value="Operadores de Transporte"
        />
        <Picker.Item label="Oficinista" value="Oficinista" />
        <Picker.Item
          label="Comerciantes y Dependientes"
          value="Comerciantes y Dependientes"
        />
        <Picker.Item
          label="Trabajador Ambulante"
          value="Trabajador Ambulante"
        />
        <Picker.Item
          label="Trabajadores en Servicios Públicos"
          value="Trabajadores en Servicios Públicos"
        />
        <Picker.Item
          label="Trabajadores en Servicios Domésticos"
          value="Trabajadores en Servicios Domésticos"
        />
        <Picker.Item
          label="Protección y Vigilancia"
          value="Protección y Vigilancia"
        />
      </Picker>
      <Picker
        selectedValue={situacionActualP}
        onValueChange={(v, i) => {
          setFieldValue('situacionActual', v);
          setSituacionActualP(v);
        }}>
        <Picker.Item label={t.schema.icet.situacionActual} value="" />
        <Picker.Item label="Estudiante" value="Estudiante" />
        <Picker.Item label="Se dedica al hogar" value="Se dedica al hogar" />
        <Picker.Item label="Jubilado/Pensionado" value="Jubilado/Pensionado" />
        <Picker.Item
          label="Incapacitado para trabajar"
          value="Incapacitado para trabajar"
        />
        <Picker.Item label="Otro" value="Otro" />
      </Picker>
      <Input
        placeholder={t.schema.icet.medioEntero}
        errorStyle={style.inputError}
        errorMessage={errors.medioEntero}
        onChangeText={handleChange('medioEntero')}
        value={values.medioEntero}
      />
      <Input
        placeholder={t.schema.icet.razonICET}
        errorStyle={style.inputError}
        errorMessage={errors.razonICET}
        onChangeText={handleChange('razonICET')}
        value={values.razonICET}
      />
      <Input
        placeholder={t.schema.icet.razonCurso}
        errorStyle={style.inputError}
        errorMessage={errors.razonCurso}
        onChangeText={handleChange('razonCurso')}
        value={values.razonCurso}
      />
      <Picker
        selectedValue={liquidarCursoP}
        onValueChange={(v, i) => {
          setFieldValue('liquidarCurso', v);
          setLiquidarCursoP(v);
        }}>
        <Picker.Item label={t.schema.icet.liquidarCurso} value="" />
        <Picker.Item label="Participante" value="Participante" />
        <Picker.Item label="La Empresa" value="La Empresa" />
        <Picker.Item
          label="Servicio Estatal de Empleo"
          value="Servicio Estatal de Empleo"
        />
        <Picker.Item label="El Plantel" value="El Plantel" />
      </Picker>
      <Input
        placeholder={t.schema.icet.otrasAreas}
        errorStyle={style.inputError}
        errorMessage={errors.otrasAreas}
        onChangeText={handleChange('otrasAreas')}
        value={values.otrasAreas}
      />
      <ButtonTab title={t.shared.submit} onPress={() => handleSubmit()} />
    </ScrollView>
  );
};

export default ICETForm;
