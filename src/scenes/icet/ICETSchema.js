import * as yup from 'yup';

import i18n from '../../config/i18n';

const {t} = i18n;

export const initialICETValues = {
  trabajaActualmente: false,
  ultimoNivel: '',
  situacionActual: '',
  otrasAreas: '',
  medioEntero: '',
  razonCurso: '',
  razonICET: '',
  liquidarCurso: '',
  antiguedad: '',
  direccionTrabajo: '',
  discapacidad: '',
  nombreEmpresa: '',
  ocupacionPrincipal: '',
  situacionTrabajo: '',
  telefonoEmpresa: '',
  taller: '',
};

export const ICETSchema = yup.object({
  taller: yup.string().required(t.shared.required),
  discapacidad: yup.string().required(t.shared.required),
  ultimoNivel: yup.string().max(15, t.shared.max).required(t.shared.required),
  //medioEntero: yup.array().of(yup.string()).required(t.shared.required),
  //razonICET: yup.array().of(yup.string()).required(t.shared.required),
  //razonCurso: yup.array().of(yup.string()).required(t.shared.required),
  //otrasAreas: yup.array().of(yup.string()).required(t.shared.required),
  liquidarCurso: yup.string().required(t.shared.required),
});
