import React from 'react';
import {View} from 'react-native';
import {Text, Card} from 'react-native-elements';

import Header from '../../components/header/Header';
import ButtonTab from '../../components/buttonTab/ButtonTab';

import i18n from '../../config/i18n';
import style from './style';
import routes from '../../config/routes';

const ThanksScene = ({navigation}) => {
  const {thanks: t} = i18n.t.scenes;
  return (
    <>
      <Header />
      <View style={style.container}>
        <Card style={style.cardContainer}>
          <Text h2 style={style.title}>
            {t.title}
          </Text>
          <Text style={style.body}>{t.body}</Text>
        </Card>
        <ButtonTab
          title={t.home}
          onPress={(props) => navigation.replace(routes.Privacy.name)}
        />
      </View>
    </>
  );
};

export default ThanksScene;
