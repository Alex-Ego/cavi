import {StyleSheet} from 'react-native';
import colors from '../../config/colors';

const style = StyleSheet.create({
  imgBg: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  logo: {
    width: '100%',
    height: '100%',
  },
  hero: {
    top: 10,
    textAlign: 'center',
    color: colors.primary,
  },
});

export default style;
