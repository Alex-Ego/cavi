import React, {useEffect} from 'react';
import {Image, ImageBackground} from 'react-native';
import {Text} from 'react-native-elements';

import routes from '../../config/routes';
import i18n from '../../config/i18n';
import style from './style';

const {splash} = i18n.t.scenes;
const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

const SplashScene = ({navigation, isLoading, data}) => {
  useEffect(() => {
    sleep(300).then(() => navigation.replace(routes.Privacy.name));
  });

  return (
    <ImageBackground
      blurRadius={1}
      style={style.imgBg}
      source={require('./splash2.jpeg')}>
      <Image source={require('./logo.png')} />
      <Text h3 style={style.hero}>
        {splash.title}
      </Text>
    </ImageBackground>
  );
};

export default SplashScene;
