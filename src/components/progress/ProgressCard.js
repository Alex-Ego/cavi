import React, {Component} from 'react';
import {View} from 'react-native';
import {Card, Text} from 'react-native-elements';

import CircularProgress from './CircularProgress';

import style from './style';

class ProgressCard extends Component {
  render() {
    return (
      <Card containerStyle={style.cardContainer}>
        <View style={style.container}>
          <CircularProgress
            size={70}
            strokeWidth={10}
            text={this.props.step}
            progressPercent={this.props.progress}
          />
          <View style={style.cardInfo}>
            <Text style={style.title}>{this.props.currentStep}</Text>
            <Text style={style.subtitle}>{this.props.nextStep}</Text>
          </View>
        </View>
      </Card>
    );
  }
}

export default ProgressCard;
