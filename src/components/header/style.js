import {StyleSheet} from 'react-native';

import colors from '../../config/colors';

const style = StyleSheet.create({
  container: {
    flex: 0.2,
    backgroundColor: colors.white,
  },
  headerBg: {
    position: 'absolute',
    alignSelf: 'flex-start',
  },
  logo: {
    position: 'absolute',
    alignSelf: 'flex-end',
    right: 20,
    top: 20,
  },
});

export default style;
