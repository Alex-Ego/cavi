import 'react-native-gesture-handler';
import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import routes from '../../config/routes';

const Stack = createStackNavigator();
const {Splash, Privacy, Wizard, Thanks} = routes;

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      isLoading: true,
    };
  }

  componentDidMount() {
    fetch('https://jsonplaceholder.typicode.com/todos/1')
      .then((response) => response.json())
      .then((json) => this.setState({data: json}))
      .catch((error) => console.error(error))
      .finally(() => {
        this.setState({isLoading: false});
      });
  }

  render() {
    const {data, isLoading} = this.state;
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName={Splash.name} headerMode="none">
          <Stack.Screen name={Splash.name}>
            {(props) => (
              <Splash.scene {...props} isLoading={isLoading} data={{...data}} />
            )}
          </Stack.Screen>
          <Stack.Screen name={Privacy.name}>
            {(props) => <Privacy.scene {...props} data={{...data}} />}
          </Stack.Screen>
          <Stack.Screen name={Wizard.name}>
            {(props) => (
              <Wizard.scene {...props} isLoading={isLoading} data={{...data}} />
            )}
          </Stack.Screen>
          <Stack.Screen name={Thanks.name} component={Thanks.scene} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
