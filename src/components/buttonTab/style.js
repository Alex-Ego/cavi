import {StyleSheet} from 'react-native';

import colors from '../../config/colors';

const style = StyleSheet.create({
  cardContainer: {
    flex: 0.1,
    paddingBottom: 30,
  },
  container: {
    flex: 1,
    flexDirection: 'row',
  },
  btn: {
    width: '100%',
    backgroundColor: colors.primary,
    color: colors.white,
  },
});

export default style;
