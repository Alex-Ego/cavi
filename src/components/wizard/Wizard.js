import React, {useState} from 'react';
import {Formik} from 'formik';

import ProgressCard from '../progress/ProgressCard';

export const Wizard = ({children, initialValues, onSubmit}) => {
  const [stepNumber, setStepNumber] = useState(0);
  const steps = React.Children.toArray(children);
  const [snapshot, setSnapshot] = useState(initialValues);

  const step = steps[stepNumber];
  const totalSteps = steps.length;
  const isLastStep = stepNumber === totalSteps - 1;

  const next = (values) => {
    setSnapshot(values);
    setStepNumber(Math.min(stepNumber + 1, totalSteps - 1));
  };

  const previous = (values) => {
    setSnapshot(values);
    setStepNumber(Math.max(stepNumber - 1, 0));
  };

  const handleSubmit = async (values, bag) => {
    console.log('on handle submit', stepNumber);
    if (step.props.onSubmit) {
      await step.props.onSubmit(values, bag);
    }
    if (isLastStep) {
      return onSubmit(values, bag);
    } else {
      bag.setTouched({});
      next(values);
    }
  };

  return (
    <Formik
      initialValues={snapshot}
      onSubmit={handleSubmit}
      validationSchema={step.props.validationSchema}>
      {(formik) => (
        <>
          <ProgressCard
            progress={step.props.progressCard.progress}
            step={step.props.progressCard.step}
            currentStep={step.props.progressCard.currentStep}
            nextStep={step.props.progressCard.nextStep}
          />
          {step}
        </>
      )}
    </Formik>
  );
};

export const WizardStep = ({children}) => children;
