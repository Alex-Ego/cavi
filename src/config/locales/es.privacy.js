const privacy = {
  title: 'TÉRMINOS Y CONDICIONES',
  meaning: '¿Qué significa?',

  section1: {
    title: 'I. Las finalidades para el uso de sus datos personales. ',
    subtext:
      'Los datos personales que recabamos de usted, los utilizaremos para las siguientes finalidades que son necesarias para la relación jurídica:',
    subsection1: {
      title: 'Datos de identificación: ',
      subtext:
        'Identificarlo; contactarlo; tener un contacto ante alguna eventualidad; perfilarlo en general y para determinar si puede cursar o mantenerse cursando algún programa; informarle y/o gestionar alguna obligación que contraiga; integrarlo en la comunidad; permitir su acceso a los edificios; utilizar las instalaciones y servicios que le proporciona la Institución; mantener comunicación respecto a las actividades y eventos relacionados a actividades escolares, extraescolares y/o que puedan ser de su interés; facilitar y permitir el desarrollo de las actividades escolares y extraescolares que se gestionan en la Institución dentro y fuera de las instalaciones; facilitar la comunicación entre los integrantes de los diferentes programas; enviar información referente a los programas en que se encuentra inscrito o información que solicite; gestión académica y administrativa; proporcionar seguridad en las instalaciones; emisión de documentos oficiales, credenciales, diplomas que se requieran en los programas; envío de documentos; integrar información para la participación en rankings nacionales e internacionales; nacionalidad para cumplir con los requerimientos legales necesarios con la finalidad de proporcionarle los servicios; gestionar la contratación de seguros necesarios; contar con los registros y proporcionarle información con respecto a su desempeño; publicación de sus datos, impresiones fotográficas, audio o video grabaciones en páginas informativas de internet, redes sociales, documentos que son informativos y/o de interés común; cumplir con los requerimientos legales dentro y fuera del territorio nacional.',
      meaning:
        'Los datos de identificación se utilizarán para ayudar a la institución para conocerlo, mantener comunicación y proporcionarle información de los cursos y/o actividades, además le permitirá el acceso y uso de las instalaciones.',
    },
    subsection2: {
      title: 'Datos Patrimoniales: ',
      subtext:
        'Atender e informar los requerimientos de pagos y cobranzas; generar y enviar facturas.',
      meaning:
        'Se utilizarán estos datos para proporcionarle un mejor servicio en el área de facturación y pagos.',
    },
    subsection3: {
      title: 'Datos Sensibles: ',
      subtext:
        'Generar un diagnóstico y el correcto tratamiento ante una incidencia médica; contar con los datos para seguimiento posterior a alguna incidencia, si se requiere; canalizarlo al lugar de su preferencia ante una incidencia médica; solicitar el apoyo que sea de su conveniencia ante una incidencia médica.',
      meaning: 'El objetivo es brindarle una mejor atención médica.',
    },
    subsection4: {
      title: 'Fines secundarios: ',
      subtext:
        'Se podrán utilizar los datos para fines estadísticos, previa disociación de estos. Video y audio grabación de cursos para su publicación previa autorización.',
      meaning:
        'Sus datos proporcionados no se asociarán al titular ni permitirá la identificación de este.',
    },
  },
  section2: {
    title: 'II. Los datos personales que utilizamos son los siguientes ',
    subtext: 'Los datos personales son obtenidos directamente del titular.',
    subsection1: {
      title: 'Datos personales: ',
      subtext:
        'Nombre completo; género; domicilio; domicilio para recibir documentación; nacionalidad; lugar y fecha de nacimiento; correo electrónico; teléfonos; edad; identificación oficial (INE o Pasaporte); R.F.C.; C.U.R.P.; fotografía; grado de estudios; título o certificados educativos; datos que lo perfilan académicamente; dominio de idiomas y sus certificados; los datos extras que se incluyan en los documentos que nos proporcione; audio y video grabación de algún curso; todos los datos que se generan por la relación jurídica; fotografías de los participantes en eventos relacionados con los programas; información referente a un evento o que se generan en el evento por la misma relación jurídica y; los datos que se generan por la relación jurídica. Para extranjeros: documentación que acredite su nacionalidad, identificación y estatus migratorio, documentos migratorios con permiso para estudiar;',
      meaning:
        'Esta información es requerida como parte de proceso de identificación.',
    },
    subsection2: {
      title: 'Datos patrimoniales: ',
      subtext:
        'Datos de Facturación; quién financia el programa; los datos extras que presente en los documentos que nos proporcione; los datos requeridos para gestionar los pagos y; los datos que se generen por la relación jurídica.',
      meaning:
        'Estos datos serán utilizados para el proceso de pago y becas, según sea necesario.',
    },
    subsection3: {
      title: 'Datos sensibles: ',
      subtext:
        'Hospital en donde se prefiere atender ante alguna eventualidad médica; nombre y datos de las personas a las que se deberá contactar ante alguna eventualidad médica; toda la información requerida para atender alguna incidencia médica; los datos generados en el tratamiento de la incidencia médica y; los datos que se generen por la relación jurídica.',
      meaning:
        'Estos datos serán utilizados para que en caso de que haya alguna emergencia médica, se pueda actuar con rapidez y seguridad.',
    },
  },
  section3: {
    title: 'III. Transferencias de sus datos personales y sus finalidades. ',
    subtext:
      'Le informamos que, en la contratación y ejecución de algunos servicios necesarios para la ejecución de la relación jurídica con usted sus datos personales pueden ser transferidos a terceros, dentro y fuera del territorio nacional, con quienes se ha contratado para que realicen ciertas tareas relacionadas con los servicios correspondientes a los programas.',
    meaning:
      'Sus datos pueden ser transferidos a otras instituciones con la finalidad de cumplir con tareas relacionadas a los cursos.',
  },
};

export default privacy;
