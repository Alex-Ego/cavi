import es from './locales/es';

const i18n = {
  defaultLocale: 'es',
  locale: 'es',
  t: {...es},
};

export default i18n;
